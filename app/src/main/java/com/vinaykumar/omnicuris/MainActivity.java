package com.vinaykumar.omnicuris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.vinaykumar.omnicuris.activity.QuestionSummary;
import com.vinaykumar.omnicuris.adapter.QuestionsAdapter;
import com.vinaykumar.omnicuris.model.Questions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Questions> questionsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private QuestionsAdapter mAdapter;
    private HashMap<String,String > userAnswerList;
    private Button btSubmit;
    private int count =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        btSubmit = (Button) findViewById(R.id.bt_submit);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userAnswerList.size()==questionsList.size()) {
                    for (int i = 0; i < questionsList.size(); i++) {
                        if (questionsList.get(i).getAnswer() == userAnswerList.get(String.valueOf(i))) {
                            count++;
                        }
                    }
                    Intent intent = new Intent(getApplicationContext(), QuestionSummary.class);
                    intent.putExtra("question_list", (Serializable) questionsList);
                    intent.putExtra("answer_list", (Serializable) userAnswerList);
                    intent.putExtra("answer_count", String.valueOf(count));
                    startActivity(intent);
                    finish();
//                    btSubmit.setText(String.valueOf(count));
                }
                else{
                    Toast.makeText(getApplicationContext(),"Kindly attempt all questions",Toast.LENGTH_LONG).show();
                }
            }
        });

        mAdapter = new QuestionsAdapter(questionsList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        userAnswerList = new HashMap<>();

        prepareMovieData();
    }

    private void prepareMovieData() {
        Questions questions = new Questions("1","Where is Dublin?", "Ireland", "Canada","USA","CHINA","Ireland");
        questionsList.add(questions);

        questions = new Questions("2","Who is vice president of india?", "Manmohan Singh", "Hamid Ansari","Ramnath Kovind","Venkaiah Naidu","Venkaiah Naidu");
        questionsList.add(questions);


        mAdapter.notifyDataSetChanged();
    }

    public void noteAnswer(int index , String answer){
        userAnswerList.put(String.valueOf(index),answer);
    }
}